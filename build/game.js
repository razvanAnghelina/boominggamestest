var events = [];

function Event(_target, _eventName, _callback) {
	this.target = _target;
	this.ctor = function () {};
	this.eventName = _eventName;
	this.callback = _callback;
}

var EventEmitter = function () {}
EventEmitter.prototype.ctor = function (options) {}

EventEmitter.prototype.on = function (_eventName, _callback) {
	var e = new Event(this, _eventName, _callback);
	events.push(e);
}

EventEmitter.prototype.off = function (_eventName, _callback) {
	var e;
	for (var i = events.length - 1; i >= 0; i--) {
		e = events[i];
		if (e.eventName === _eventName && e.callback === _callback && e.target === this) {
			delete events[i];
			events[i] = null;
			events.splice(i, 1);
		}
	}
}

EventEmitter.prototype.dispatch = function (_eventName, data) {
	var e;
	for (var i = 0; i < events.length; i++) {
		e = events[i];
		if (e.eventName === _eventName && e.target === this) {
			if (e.callback) {
				e.callback(data);
			}
		}
	}
}

Function.prototype.clone = function () {
	var that = this;
	var temp = function temporary() {
		return that.apply(this, arguments);
	};
	for (var key in this) {
		if (this.hasOwnProperty(key)) {
			temp[key] = this[key];
		}
	}
	return temp;
};

EventEmitter.extend = function (ob) {
	var _super = this.prototype;
	var prototype = Object.create(_super);

	function Class(a1, a2, a3, a4, a5, a6) {
		this.ctor(a1, a2, a3, a4, a5, a6);
	}

	prototype.super = prototype.ctor.clone();

	for (var i in ob) {
		prototype[i] = ob[i];
	}

	Class.prototype = prototype;
	Class.extend = this.extend;
	return Class;
}

var GameEvents = {
    PRELOAD_START : "preloadStart",
    PRELOAD_COMPLETE : "preloadComplete",
    PRELOAD_PROGRESS : "preloadProgress",
    UPDATE : "update",
    RESIZE : "resize",
    TILE_BREAKED : "tileBreaked"
};
var Position = function (_x, _y) {
    var ob = {x:0,y:0};

	if (_x !== undefined) {
		ob.x = _x;
	}

	if (_y !== undefined) {
		ob.y = _y;
	}
    
    ob.toString = function(){
        return "{x:"+this.x+",y:"+this.y+"}";
    }
    
    return ob;
};

var Utils = {};

Utils.addZeros = function(nr,total){
    try{
        if(nr.length < total){
            var diff = total-nr.length;
            var zeros = "";
            for(var i=0; i<diff; i++){
                zeros += "0";
            }
            nr = zeros+nr;
        }
    }catch(e){
        throw new Error(e.message);
    }
    return nr;
}
var lastTime = 0;
var vendors = ['ms', 'moz', 'webkit', 'o'];
for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
}

if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };

if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };



var Engine = {};
Engine.loop_id = 0;
Engine.last_time = 0;
   
var longPrss = 0;

Engine.addTouchEvents = function(elem){
    var _isMobile = isMobile.any();
	
    var _click = _isMobile ? "tap" : "click";
    var _mouseOver = _isMobile ? "touchstart" : "mouseover";
    var _mouseMove = _isMobile ? "touchmove" : "mousemove";
    var _mouseOut = _isMobile ? "touchend" : "mouseout";
    var _mouseDown = _isMobile ? "touchstart" : "mousedown";
    var _mouseUp = _isMobile ? "touchend" : "mouseup";
    var _mouseUpOutside = _isMobile ? "touchendoutside" : "mouseup";

    var longPrss ;
    elem.interactive = true;
    elem.buttonMode  = true;

    elem._mouseOver = function(e){
        if(elem.onMouseOver !== undefined){
            elem.onMouseOver(e);
        }
    }

    elem._mouseOut = function(e){
        if(elem.onMouseOut !== undefined){
            elem.onMouseOut(e);
        }
    }

    elem._mouseDown = function(e){
        if(elem.onMouseDown !== undefined){
            elem.onMouseDown(e);
        }
    }

    elem._mouseMove = function(e){
        if(elem.onMouseMove !== undefined){
            elem.onMouseMove(e);
        }
    }
    
    elem._mouseUp = function(e){
        if(elem.onMouseUp !== undefined){
            elem.onMouseUp(e);
        }
    }
    
    elem._click = function(e){
        if(elem.onClick !== undefined){
            elem.onClick(e);
        }
    }

    if(_isMobile){
        elem._mouseUpOutside = function(e){
            if(elem.onMouseUp !== undefined){
                elem.onMouseUp(e);
            }
        }
    }

    if(_isMobile){
        elem.on(_mouseDown,elem._mouseDown);
        elem.on(_mouseOut,elem._mouseOut);
        elem.on(_mouseUp,elem._mouseUp);
        elem.on(_mouseMove,elem._mouseMove);
        elem.on(_mouseUpOutside,elem._mouseUpOutside);
        elem.on(_click,elem._click);
    }else{
        elem.on(_mouseDown,elem._mouseDown);
        elem.on(_mouseOver,elem._mouseOver);
        elem.on(_mouseOut,elem._mouseOut);
        elem.on(_mouseMove,elem._mouseMove);
        elem.on(_mouseUp,elem._mouseUp);
        elem.on(_click,elem._click);
    }
}
	
Engine.removeTouchEvents = function(elem,f){
    var _isMobile = isMobile.any();
    var _click = _isMobile ? "tap" : "click";
    var _mouseOver = _isMobile ? "touchstart" : "mouseover";
    var _mouseOut = _isMobile ? "touchend" : "mouseout";
    var _mouseDown = _isMobile ? "touchstart" : "mousedown";
    var _mouseUpOutside = _isMobile ? "touchendoutside" : "mouseup";

    elem.interactive = false;
    elem.buttonMode  = false;

    if(_isMobile){
        elem.off(_mouseDown,elem._mouseDown);
        elem.off(_mouseOut,elem._mouseOut);
        elem.off(_mouseUp,elem._mouseUp);
        elem.off(_mouseUpOutside,elem._mouseUpOutside);
        elem.off(_click,elem._click);
    }else{
        elem.off(_mouseDown,elem._mouseDown);
        elem.off(_mouseOver,elem._mouseOver);
        elem.off(_mouseOut,elem._mouseOut);
        elem.off(_mouseUp,elem._mouseUp);
        elem.off(_click,elem._click);
    }
}

window.addTouchEvents = Engine.addTouchEvents;
window.removeTouchEvents = Engine.removeTouchEvents;

    
Engine.startAnimation = function(){
    var t = this;
    if(t.stopAnim){
        return;
    }
    t.animStarted = true;
    
    t.loop_id = window.requestAnimationFrame(function(){ t.startAnimation(); });
    var d = new Date();
    var nt = d.getTime();

    if(t.last_time == 0){
        t.last_time = nt;
    }

    var dif = nt - t.last_time;
    t.last_time = nt;
    if(t.update !== null){
        t.update(dif);
    }

}
	
Engine.stopAnim = false;
	
Engine.stopAnimation = function(){
    window.cancelAnimationFrame(this.loop_id);
}

window.isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    iPhone: function() {
        return navigator.userAgent.match(/iPhone/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
		/*
		if(navigator.userAgent.match(/Windows Phone/i)){
			return false;
		}
		*/
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    }
};
var GC = {};
GC.rows = 8;
GC.cols = 8;
GC.tileSize = 74.1;
GC.padding = 3;

GC.GAME_IS_TRANSPARENT = true;
GC.size = {width:1024,height:768};
GC.imagesFolder = "images/";

GC.assets = {
    "background":"images/background.png",
    "border":"images/border.png"
};

GC.ORIENTATION_LIST = {
    LANDSCAPE:"landscape",
    PORTRAIT:"portrait"
};
var Game = EventEmitter.extend(
    {
        container:null,
        renderer:null,
        stage:null,
        canvas:null,
        canvasID:"game",
        scale:1,
        orientation:"",
        loader:null,
        loaded:false,
        gameContainer:null,
        loadingScreen:null,
        imagesFolder:"",
        score:0,
        scoreLabel:null,
        
        tilePuzzle:null,
        tilesContainer:null,
        tilesContainerMask:null,
        
        ctor:function(options){
            for(var i in options){
                if(this[i] !== undefined){
                    this[i] = options[i];
                }
            }
            
            window.getImage = this.getImage;
            window.game = this;
        },
        
        init:function(){
            if(this.container === null){
                this.container = document.body;
            }
            
            this.stage = new PIXI.Container();
            this.loader = new PIXI.loaders.Loader();
            
            window.loader = this.loader;
            
            this.container.innerHTML = '<canvas id="' + this.canvasID + '" tabindex="2"></canvas>';
            this.renderer = PIXI.autoDetectRenderer(GC.size.width, GC.size.height, { backgroundColor: 0x000000, transparent: GC.GAME_IS_TRANSPARENT, view: document.getElementById(this.canvasID) });
            
            this.canvas = this.renderer.view;
            
            var t = this;
            Engine.update = function (dt) { t.Update(dt) };
            Engine.startAnimation();

            var t = this;
            window.addEventListener("resize", function () {
                t.onResize();
            });
             window.addEventListener("orientationchange", function () {
                t.onResize();
            });
            this.onResize();
            
            this.initGameObjects();
            
            var loading = new LoadingScreen(this.loadingScreen);
            
            this.startLoadingSequence();
            
            
        },
        
        initGameObjects:function(){
            this.gameContainer = new PIXI.Container();
            this.stage.addChild(this.gameContainer);
            
            this.loadingScreen = new PIXI.Container();
            this.stage.addChild(this.loadingScreen);
            
        },
        
        onLoadDone:function(){
            this.createGameLayout();
            
            var t = this;
            
            setTimeout(function(){
                TweenLite.to(t.loadingScreen,1,{alpha:0});
            },500);
            
        },
        
        createGameLayout:function(){
            var bg = new PIXI.Sprite(getImage("background"));
            this.gameContainer.addChild(bg);
            
            this.tilesContainer = new PIXI.Container();
            this.gameContainer.addChild(this.tilesContainer);
            
            this.tilesContainerMask = new PIXI.Graphics();
            
            this.gameContainer.addChild(this.tilesContainerMask);
            this.tilesContainer.mask = this.tilesContainerMask;
            
            this.scoreLabel = new PIXI.Text();
            this.scoreLabel.style.fontFamily = "Roboto";
            this.scoreLabel.style.fontSize = 30;
            this.scoreLabel.style.fill = 0xffffff;
            this.scoreLabel.anchor.x = .5;
            
            this.scoreLabel.x = 172;
            this.scoreLabel.y = 170;
            
            this.gameContainer.addChild(this.scoreLabel);
            
            this.tilePuzzle = new TilePuzzle();
            this.tilePuzzle.container = this.tilesContainer;
            this.tilePuzzle.container.x = 380;
            this.tilePuzzle.container.y = 112;
			this.tilePuzzle.create();
            
            var t = this;
			this.tilePuzzle.on(GameEvents.TILE_BREAKED,function(scoreToAdd){ t.onTileBreaked(scoreToAdd); })
            
            this.tilesContainerMask.x = this.tilePuzzle.container.x - GC.tileSize/2;
            this.tilesContainerMask.y = this.tilePuzzle.container.y - GC.tileSize/2;
            this.tilesContainerMask.beginFill(0x000000,1);
            this.tilesContainerMask.drawRect(0,0,GC.cols*(GC.tileSize+GC.padding),GC.rows*(GC.tileSize+GC.padding));
            
            
            this.updateScore();
            
        },
        
        onTileBreaked:function(scoreToAdd){
            this.score += scoreToAdd;
            this.updateScore();
        },
        
        updateScore:function(){
            this.scoreLabel.text = Utils.addZeros(Math.round(this.score).toString(),6);
        },
        
        onResize:function(){
            var new_scale = 1;
            
            var STAGE_WIDTH = window.innerWidth;
            var STAGE_HEIGHT = window.innerHeight;

            var rap = GC.size.width / GC.size.height;
            var ww = STAGE_WIDTH;
            var hh = STAGE_WIDTH / rap;

            var _sw = STAGE_WIDTH;
            var _sh = STAGE_HEIGHT;

            this.orientation = _sw > _sh ? GC.ORIENTATION_LIST.LANDSCAPE : GC.ORIENTATION_LIST.PORTRAIT;

            var rap2 = _sw / _sh;

            var pixelRatio = 1;

            if (window.devicePixelRatio > 1) {
                pixelRatio = window.devicePixelRatio;
            }

            this.canvas.width = _sw * pixelRatio;
            this.canvas.height = _sh * pixelRatio;
            this.canvas.style.width = _sw + 'px';
            this.canvas.style.height = _sh + 'px';

            var nh = _sw / rap;
            new_scale = _sw / GC.size.width;

            if (this.orientation == GC.ORIENTATION_LIST.LANDSCAPE && nh > _sh) {
                new_scale = _sh / GC.size.height;
            }

            this.stage.scale.x = (new_scale * pixelRatio);
            this.stage.scale.y = (new_scale * pixelRatio);
            this.stage.initScale = (new_scale * pixelRatio);
            this.stage.x = (this.canvas.width - GC.size.width * this.stage.scale.x) / 2;
            this.stage.initX = (this.canvas.width - GC.size.width * this.stage.scale.x) / 2;
            this.stage.y = 0;
            this.stage.initY = 0;

            this.renderer.resize(this.canvas.width, this.canvas.height);
            this.scale = new_scale;
            this.dispatch(GameEvents.RESIZE, { screenWidth: STAGE_WIDTH, screenHeight: STAGE_HEIGHT, gameScale: new_scale, pixelRatio: pixelRatio, orientation: this.orientation });
            
        },
        
        Update: function (dt) {
            this.renderer.render(this.stage);
            this.dispatch(GameEvents.UPDATE);
        },
        
        getImage: function (tex) {
            var t = loader.resources[tex];
            if (t !== undefined) {
                return t.texture;
            }
            
            
            try{
                 t = PIXI.Texture.fromFrame(tex + '.png');
            }catch(e){
                try{
                    t = PIXI.Texture.fromImage(game.imagesFolder + tex + '.png'); 
                }catch(e){
                    Log("texture not found: ",tex);
                }
            }
            
            return t;
        },
        
        startLoadingSequence : function(){
            
            for (var i in GC.assets) {
                this.loader.add(i, GC.assets[i]);
            }

            var t = this;
            this.loader.on("progress", function (e) { t.dispatch(GameEvents.PRELOAD_PROGRESS, e); });
            this.loader.once("complete", function (e) { t.loaded = true; t.dispatch(GameEvents.PRELOAD_COMPLETE); t.onLoadDone();  });
            this.loader.load();  
        }
        
    });
var LoadingScreen = EventEmitter.extend(
    {
        container:null,
        bar_empty:null,
        bar_full:null,
        
        barWidth:200,
        barHeight:10,
        barEmptyColor:0xcccccc,
        barFullColor:0xcc0000,
        
        ctor:function(container){
            this.container = container;
            
            var bg = new PIXI.Sprite(getImage("loading_bg"));
            
            this.container.addChild(bg);
            
            this.bar_empty = new PIXI.Graphics();
            this.bar_full = new PIXI.Graphics();
            
            this.container.addChild(this.bar_empty);
            this.container.addChild(this.bar_full);
            
            this.bar_empty.beginFill(this.barEmptyColor,1);
            this.bar_empty.drawRect(0,0,this.barWidth,this.barHeight);
            
            this.bar_empty.x = this.bar_full.x = (GC.size.width - this.barWidth)/2;
            this.bar_empty.y = this.bar_full.y = (GC.size.height - this.barHeight)/2 + 70;
            
            var t = this;
            game.on(GameEvents.PRELOAD_PROGRESS,function(p){ t.onProgress(p); });
            
        },
        
        onProgress:function(p){
            
            this.bar_full.clear();
            this.bar_full.beginFill(this.barFullColor,1);
            this.bar_full.drawRect(0,0,this.barWidth * p.progress/100,this.barHeight);
            
        }
        
    });

var Tile = EventEmitter.extend(
    {
		x:0,
        y:0,
        
		fixedPos : null,
		finalPosition : null,
		positionsToMove : 0,
		
		nextPositions : [],
		animPoints : [],
		time : 0,
		doneAnim : false,
		
		type: 0,
		breaked : false,
		
		canMatch : true,
		
		moved : false,
		ignoreBreak : false,
		real_y: 0,
		noHit: 0,
		
		dirToMove : "-",
		
		solutionTypeVertical : false,
		solutionTypeOrizontal : false,
		
		graphic:null,
		border:null,
		bg:null,
		
        ctor:function(color){
            
            this.fixedPos = new Position();
            this.finalPosition = new Position();
            
            this.graphic = new PIXI.Container();
            
            this.bg = new PIXI.Graphics();
            this.bg.beginFill(color,1);
            this.bg.drawRect(0,0,GC.tileSize,GC.tileSize);
            
            this.border = new PIXI.Sprite(getImage("border"));
            this.border.visible = false;
            this.border.pivot.x = this.border.width/2;
            this.border.pivot.y = this.border.height/2;
            
            this.border.x = GC.tileSize/2;
            this.border.y = GC.tileSize/2;
            
            this.graphic.addChild(this.bg);
            this.graphic.addChild(this.border);
            
            this.graphic.pivot.x = GC.tileSize/2;
            this.graphic.pivot.y = GC.tileSize/2;
            
            
        },
        
        showBorder:function(){
            this.border.visible = true;
        },
        
        hideBorder:function(){
            this.border.visible = false;
        },
        
        setColor:function(color){
            this.bg.clear();
            this.bg.beginFill(color,1);
            this.bg.drawRect(0,0,GC.tileSize,GC.tileSize);
        },
        
		hit:function(){
			this.breaked = true;
		}
	});	


var TilePuzzle = EventEmitter.extend(
    {
        
        tiles:[],
		container:null,
		tileSize:GC.tileSize,
		padding:GC.padding,
		colors:[0x0099CC, 0x99FF00, 0xFF33FF, 0xFFFF33, 0xcc0000],
		
		rows:GC.rows,
		cols:GC.cols,
        
        removeTiles:[],
		animTiles:[],
		
		firstSelectedTile:null,
		secondSelectedTile:null,
		
		canMove:true,

		firstSelectedTilePos:null,
		secondSelectedTilePos:null,
        
        tilesToMove:[],
        
        // game constructor
        ctor:function(options){
            for(var i in options){
                if(this[i] !== undefined){
                    this[i] = options[i];
                }
            }
        },
        
        create:function(){
			var i;
			var j;
            
            this.firstSelectedTilePos = new Position();
		    this.secondSelectedTilePos = new Position();
			
			var type = 0;
			
            window.puzzleEngine = this;
            
			var fixedFound = false;
            
            addTouchEvents(this.container);
            var _this = this;
            
            this.container.onClick = function(e){
                _this.onTileDown(e);
            }
			
			for(i = 0; i<this.rows; i++){
				this.tiles[i] = [];
                
				for(j = 0; j<this.cols; j++){
					
                    
                    type = this.getRandomType();
                    
					var t = new Tile(this.colors[type]);
					this.tiles[i][j] = t;
						
					t.type = type;
					
					this.container.addChild(t.graphic);
					t.graphic.width = this.tileSize;
					t.graphic.height = this.tileSize;
					t.graphic.x = j * (this.tileSize+this.padding);
					t.graphic.y = (i - this.rows) * (this.tileSize+this.padding);
					t.graphic.tile = t;
					t.x = j;
					t.y = i;
					t.fixedPos.x = j;
					t.fixedPos.y = i;
                    t.graphic.alpha = 0;
				}
			}
            
			this.shuffleBoard();
            
            setTimeout(function(){
               _this.showInitialAnimation(); 
            },1000);
            
		},
        
        getRandomType:function(){
             return Math.floor(Math.random()*this.colors.length);
        },
        
        showInitialAnimation:function(){
            var tile,i,j;
            var t = this;
            
            var timeline = new TimelineLite({onComplete:function(){ t.canMove = true; }});
            this.canMove = false;
            
            for(i = 0; i<this.rows; i++){
				for(j = 0; j<this.cols; j++){
                    tile = this.tiles[i][j];
                    timeline.to(tile.graphic,1,{alpha:1,y: i * (this.tileSize+this.padding),ease:Quad.easeOut},(this.rows-i+j)/30);
                }
            }
        },
		
		shuffleBoard:function(){
			var sols = this.getMatches();
			var i;
			var j;
			var type = 0;
			while(sols.length > 0){
				for(i=0; i<this.tiles.length; i++){
					for(j=0; j<this.tiles[0].length; j++){
						type = this.getRandomType();
						this.tiles[i][j].type = type;
						this.tiles[i][j].setColor(this.colors[type]);
					}
				}
				sols = this.getMatches();
			}
			
			for(i=0; i<this.tiles.length; i++){
				for(j=0; j<this.tiles[0].length; j++){
					this.tiles[i][j].setColor(this.colors[this.tiles[i][j].type]);
				}
			}
			
		},
		
		getMatches:function(){
			var i = 0;
			var j = 0;
			var k = 0;
			
			var count = 0;
			var lastType = 0;
			var t;
			
			var _type = 0;
			var solutions = [];
			var sol;
            
			/// verificare pe orizontala
			for(i=0; i<this.tiles.length; i++){
				lastType = 0;
				sol = [];
				_type = 0;
				for(j=0; j<this.tiles[0].length; j++){
					t = this.tiles[i][j];
					if(t.breaked){
						////////
						if(sol.length >= 3){
							solutions.push(sol);
						}
						sol = [];
						//////////
						continue;
					}
					_type = t.type;
					if(_type != lastType || j == this.tiles[0].length - 1){
						if(_type == lastType && j == this.tiles[0].length - 1){
							sol.push(t);
						}
						lastType = _type;						
						if(sol.length >= 3){
							solutions.push(sol);
						}
						sol = [];
					}
					sol.push(t);
				}
			}
			/////// end on this.rows
			////////////////////////////////////////
			
			/// verificare pe verticala
			for(i=0; i<this.tiles[0].length; i++){
				lastType = 0;
				sol = [];
				_type = 0;
				for(j=0; j<this.tiles.length; j++){
					t = this.tiles[j][i];
					if(t.breaked){
						////////
						if(sol.length >= 3){
							solutions.push(sol);
							for(k=0; k<sol.length; k++){
								sol[k].solutionTypeVertical = true;
							}
						}
						//////////
						sol = [];
						continue;
					}
					_type = t.type;
					if(_type != lastType || j == this.tiles.length - 1){
						if(_type == lastType && j == this.tiles.length - 1){
							sol.push(t);
							
						}
						lastType = _type;
						if(sol.length >= 3){
							solutions.push(sol);
							for(k=0; k<sol.length; k++){
								sol[k].solutionTypeVertical = true;
							}
						}
						sol = [];
					}
					sol.push(t);
				}
			}
			/////// end on columns
			////////////////////////////////////////
			
			return solutions;
		},
		
		onTileDown:function(e){
            
			if(!this.canMove){
				return;
			}
            
            var position = this.container.toLocal(e.data.global);
            position.x += this.tileSize/2;
            position.y += this.tileSize/2;
            
            var _x = Math.floor(position.x / (GC.tileSize + GC.padding));
            var _y = Math.floor(position.y / (GC.tileSize + GC.padding));
            
            if(_x < 0){
                _x = 0;
            }
            
            if(_y < 0){
                _y = 0;
            }
            
            if(_x > this.cols.length - 1){
                _x = this.cols.length - 1;
            }
            
            if(_y > this.rows.length - 1){
                _y = this.rows.length - 1;
            }
            
			var t = this.tiles[_y][_x];
			t.graphic.parent.setChildIndex(t.graphic,t.graphic.parent.numChildren-1);
            
			if(t == this.firstSelectedTile){
				return;
			}
			
			t.showBorder();
			
			if(this.firstSelectedTile == null){
				this.firstSelectedTile = t;
				this.firstSelectedTilePos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
			}else if(this.secondSelectedTile == null){
				if(this.isNeighbor(t,this.firstSelectedTile)){
					this.secondSelectedTile = t;
					this.secondSelectedTilePos = new Position(this.secondSelectedTile.x,this.secondSelectedTile.y);
					this.swapSelectedTiles();
				}else{
					this.firstSelectedTile.hideBorder();
					this.firstSelectedTile = t;
					this.firstSelectedTilePos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
				}
			}
		},
		
		isNeighbor:function(t1,t2){
			return (Math.abs(t1.x - t2.x) < 2 && Math.abs(t1.y - t2.y) < 1) || (Math.abs(t1.y - t2.y) < 2 && Math.abs(t1.x - t2.x) < 1);
		},
		
		swapSelectedTiles : function(){
			this.canMove = false;
			var t1pos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
            
            
			this.firstSelectedTile.x = this.secondSelectedTile.x;
			this.firstSelectedTile.y = this.secondSelectedTile.y;
			
			this.tiles[this.firstSelectedTile.y][this.firstSelectedTile.x] = this.firstSelectedTile;
			
			this.secondSelectedTile.x = t1pos.x;
			this.secondSelectedTile.y = t1pos.y;
			
			this.tiles[this.secondSelectedTile.y][this.secondSelectedTile.x] = this.secondSelectedTile;
			
            var t = this;
			TweenMax.to(this.firstSelectedTile.graphic,.4,{x:this.secondSelectedTile.graphic.x,y:this.secondSelectedTile.graphic.y});
			TweenMax.to(this.secondSelectedTile.graphic,.5,{x:this.firstSelectedTile.graphic.x,y:this.firstSelectedTile.graphic.y,onComplete:function(){ t.checkTilesMove(); }});
            
		},
		
		
		hitTile : function(t){
			if(t.ignoreBreak || t.breaked){
				return;
			}
			t.hit();
			if(t.breaked && t.graphic != null){
				this.animTiles.push(t.graphic);
				this.removeTiles.push(t);
			}
		},

		
		checkTilesMove : function(){

			var sols = this.getMatches();
			
			if(sols.length == 0){
				if(this.firstSelectedTile != null){
					TweenMax.to(this.firstSelectedTile.graphic,.4,{x:this.secondSelectedTile.graphic.x,y:this.secondSelectedTile.graphic.y});
					TweenMax.to(this.secondSelectedTile.graphic,.4,{x:this.firstSelectedTile.graphic.x,y:this.firstSelectedTile.graphic.y});
					
					this.firstSelectedTile.x = this.firstSelectedTile.fixedPos.x;
					this.firstSelectedTile.y = this.firstSelectedTile.fixedPos.y;
					this.tiles[this.firstSelectedTile.y][this.firstSelectedTile.x] = this.firstSelectedTile;
					
					this.secondSelectedTile.x = this.secondSelectedTile.fixedPos.x;
					this.secondSelectedTile.y = this.secondSelectedTile.fixedPos.y;
					this.tiles[this.secondSelectedTile.y][this.secondSelectedTile.x] = this.secondSelectedTile;
				}
				
				this.canMove = true;
				
			}else{
				this.destroySolutions(sols);
			}
			
			if(this.firstSelectedTile != null){
				this.firstSelectedTile.hideBorder();
				this.secondSelectedTile.hideBorder();
			}
			
			this.firstSelectedTile = null;
			this.secondSelectedTile = null;
		},
            
		destroySolutions : function (sols){
			var i;
			var j;
			
			this.animTiles.length = 0;
			
			for(i=0; i<sols.length; i++){
				var index = 0;

				/////////////////
				
				for(j=0; j<sols[i].length; j++){
					if(this.animTiles.indexOf(sols[i][j].graphic) == -1){
						sols[i][j].hit();
				
						if(sols[i][j].breaked && this.animTiles.indexOf(sols[i][j].graphic) == -1){
							this.animTiles.push(sols[i][j].graphic);
							this.removeTiles.push(sols[i][j]);
						}
					}
				}
			}
			
			this.showDestroyAnimation();
			
		},
		
		showDestroyAnimation : function(){
            var t = this;
            var timeline = new TimelineLite({onComplete:function(){ t.refillEmptySpaces(); }});
            for(var i=0; i<this.animTiles.length; i++){
                timeline.to(this.animTiles[i],.3,{alpha:0,ease:Quad.easeOut},0);
                timeline.to(this.animTiles[i].scale,.3,{x:0,y:0,ease:Quad.easeOut},0);
                this.dispatch(GameEvents.TILE_BREAKED,50);
            }
            
            this.animTiles.length = 0;
		},
		
		
		refillEmptySpaces : function(){
            
			var i = 0;
			var j;
			var count = 0;
			
            var _x = 0, _y = 0;
            var emptyTiles = [];
            
            for(_x=0; _x<this.cols; _x++){
                var moveDown = 0;
                emptyTiles[_x] = [];
                
                for(_y=this.rows-1; _y>=0; _y--){
                    if(this.tiles[_y][_x].breaked){
                        moveDown++;
                        emptyTiles[_x].push(_y);
                    }else if(moveDown > 0){
                        this.tilesToMove.push(this.tiles[_y][_x]);
                        this.tiles[_y][_x].y += moveDown;
                        this.tiles[_y][_x].fixedPos.y = this.tiles[_y][_x].y;
                    }
                    
                }
            }
            
            for(i=0; i<this.cols; i++){
                for(j=0; j<emptyTiles[i].length; j++){
                    _y = emptyTiles[i][j];
                    _x = i;
                    
                    this.tiles[_y][_x].breaked = false;
                    this.tiles[_y][_x].graphic.alpha = 1;
                    this.tiles[_y][_x].graphic.scale.x = this.tiles[_y][_x].graphic.scale.y = 1;
                    var new_type = this.getRandomType();
                    this.tiles[_y][_x].graphic.y = -(j+1)*(this.tileSize+this.padding);
                    this.tiles[_y][_x].type = new_type;
                    this.tiles[_y][_x].setColor(this.colors[new_type]);
                    this.tilesToMove.push(this.tiles[_y][_x]);
                    this.tiles[_y][_x].y = emptyTiles[i].length - 1 - j;
                    this.tiles[_y][_x].fixedPos.y = this.tiles[_y][_x].y;
                }
            }
            
			this.removeTiles.length = 0;
            
			if(this.tilesToMove.length > 0){
				this.showRefillAnimation();
			}else{
				this.checkIfWeCanMoveAgain();
			}
			
			
		},
		
        
		showRefillAnimation : function(){
            var t = this;
            var timeline = new TimelineLite({onComplete:function(){ t.checkIfWeCanMoveAgain(); }});
            for(var i=0; i<this.tilesToMove.length; i++){
                timeline.to(this.tilesToMove[i].graphic,.3,{y:this.tilesToMove[i].y*(this.tileSize+this.padding),ease:Quad.easeOut},0);
                this.tiles[this.tilesToMove[i].y][this.tilesToMove[i].x] = this.tilesToMove[i];
            }
            
            this.tilesToMove.length = 0;
		},
		
		checkIfWeCanMoveAgain : function(){
			var i = 0;
			var j = 0;
			
			this.firstSelectedTilePos.x = this.firstSelectedTilePos.y = this.secondSelectedTilePos.x = this.secondSelectedTilePos.y = -1;
			
			this.tilesToMove.length = 0;
			this.firstSelectedTile = this.secondSelectedTile = null;
			
			this.checkTilesMove();
		}
    }
);
    

(function () {
	var game = new Game({
			container: document.getElementById("container"),
            imagesFolder:GC.imagesFolder
		});
	game.init();
})();
