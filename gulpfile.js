var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');

gulp.task('default', function() {
  return gulp.src([
					'src/engine/EventEmitter.js',
					'src/engine/Events.js',
                    'src/utils/Position.js',
                    'src/utils/Utils.js',
					'src/engine/AnimationEngine.js',
					'src/engine/Renderer.js',
					'src/config/Config.js',
					'src/display/Game.js',
					'src/display/Loading.js',
					'src/display/Tile.js',
					'src/display/TilePuzzle.js',
					'src/main.js'
					])
    .pipe(concat('game.js'))
	//.pipe(uglify())
	.pipe(minify())
    .pipe(gulp.dest('build/'))
});