var GC = {};
GC.rows = 8;
GC.cols = 8;
GC.tileSize = 74.1;
GC.padding = 3;

GC.GAME_IS_TRANSPARENT = true;
GC.size = {width:1024,height:768};
GC.imagesFolder = "images/";

GC.assets = {
    "background":"images/background.png",
    "border":"images/border.png"
};

GC.ORIENTATION_LIST = {
    LANDSCAPE:"landscape",
    PORTRAIT:"portrait"
};