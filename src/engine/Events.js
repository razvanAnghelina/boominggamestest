var GameEvents = {
    PRELOAD_START : "preloadStart",
    PRELOAD_COMPLETE : "preloadComplete",
    PRELOAD_PROGRESS : "preloadProgress",
    UPDATE : "update",
    RESIZE : "resize",
    TILE_BREAKED : "tileBreaked"
};