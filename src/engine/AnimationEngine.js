var lastTime = 0;
var vendors = ['ms', 'moz', 'webkit', 'o'];
for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
}

if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };

if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };



var Engine = {};
Engine.loop_id = 0;
Engine.last_time = 0;
   
var longPrss = 0;

Engine.addTouchEvents = function(elem){
    var _isMobile = isMobile.any();
	
    var _click = _isMobile ? "tap" : "click";
    var _mouseOver = _isMobile ? "touchstart" : "mouseover";
    var _mouseMove = _isMobile ? "touchmove" : "mousemove";
    var _mouseOut = _isMobile ? "touchend" : "mouseout";
    var _mouseDown = _isMobile ? "touchstart" : "mousedown";
    var _mouseUp = _isMobile ? "touchend" : "mouseup";
    var _mouseUpOutside = _isMobile ? "touchendoutside" : "mouseup";

    var longPrss ;
    elem.interactive = true;
    elem.buttonMode  = true;

    elem._mouseOver = function(e){
        if(elem.onMouseOver !== undefined){
            elem.onMouseOver(e);
        }
    }

    elem._mouseOut = function(e){
        if(elem.onMouseOut !== undefined){
            elem.onMouseOut(e);
        }
    }

    elem._mouseDown = function(e){
        if(elem.onMouseDown !== undefined){
            elem.onMouseDown(e);
        }
    }

    elem._mouseMove = function(e){
        if(elem.onMouseMove !== undefined){
            elem.onMouseMove(e);
        }
    }
    
    elem._mouseUp = function(e){
        if(elem.onMouseUp !== undefined){
            elem.onMouseUp(e);
        }
    }
    
    elem._click = function(e){
        if(elem.onClick !== undefined){
            elem.onClick(e);
        }
    }

    if(_isMobile){
        elem._mouseUpOutside = function(e){
            if(elem.onMouseUp !== undefined){
                elem.onMouseUp(e);
            }
        }
    }

    if(_isMobile){
        elem.on(_mouseDown,elem._mouseDown);
        elem.on(_mouseOut,elem._mouseOut);
        elem.on(_mouseUp,elem._mouseUp);
        elem.on(_mouseMove,elem._mouseMove);
        elem.on(_mouseUpOutside,elem._mouseUpOutside);
        elem.on(_click,elem._click);
    }else{
        elem.on(_mouseDown,elem._mouseDown);
        elem.on(_mouseOver,elem._mouseOver);
        elem.on(_mouseOut,elem._mouseOut);
        elem.on(_mouseMove,elem._mouseMove);
        elem.on(_mouseUp,elem._mouseUp);
        elem.on(_click,elem._click);
    }
}
	
Engine.removeTouchEvents = function(elem,f){
    var _isMobile = isMobile.any();
    var _click = _isMobile ? "tap" : "click";
    var _mouseOver = _isMobile ? "touchstart" : "mouseover";
    var _mouseOut = _isMobile ? "touchend" : "mouseout";
    var _mouseDown = _isMobile ? "touchstart" : "mousedown";
    var _mouseUpOutside = _isMobile ? "touchendoutside" : "mouseup";

    elem.interactive = false;
    elem.buttonMode  = false;

    if(_isMobile){
        elem.off(_mouseDown,elem._mouseDown);
        elem.off(_mouseOut,elem._mouseOut);
        elem.off(_mouseUp,elem._mouseUp);
        elem.off(_mouseUpOutside,elem._mouseUpOutside);
        elem.off(_click,elem._click);
    }else{
        elem.off(_mouseDown,elem._mouseDown);
        elem.off(_mouseOver,elem._mouseOver);
        elem.off(_mouseOut,elem._mouseOut);
        elem.off(_mouseUp,elem._mouseUp);
        elem.off(_click,elem._click);
    }
}

window.addTouchEvents = Engine.addTouchEvents;
window.removeTouchEvents = Engine.removeTouchEvents;

    
Engine.startAnimation = function(){
    var t = this;
    if(t.stopAnim){
        return;
    }
    t.animStarted = true;
    
    t.loop_id = window.requestAnimationFrame(function(){ t.startAnimation(); });
    var d = new Date();
    var nt = d.getTime();

    if(t.last_time == 0){
        t.last_time = nt;
    }

    var dif = nt - t.last_time;
    t.last_time = nt;
    if(t.update !== null){
        t.update(dif);
    }

}
	
Engine.stopAnim = false;
	
Engine.stopAnimation = function(){
    window.cancelAnimationFrame(this.loop_id);
}

window.isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    iPhone: function() {
        return navigator.userAgent.match(/iPhone/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
		/*
		if(navigator.userAgent.match(/Windows Phone/i)){
			return false;
		}
		*/
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    }
};