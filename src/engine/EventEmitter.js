var events = [];

function Event(_target, _eventName, _callback) {
	this.target = _target;
	this.ctor = function () {};
	this.eventName = _eventName;
	this.callback = _callback;
}

var EventEmitter = function () {}
EventEmitter.prototype.ctor = function (options) {}

EventEmitter.prototype.on = function (_eventName, _callback) {
	var e = new Event(this, _eventName, _callback);
	events.push(e);
}

EventEmitter.prototype.off = function (_eventName, _callback) {
	var e;
	for (var i = events.length - 1; i >= 0; i--) {
		e = events[i];
		if (e.eventName === _eventName && e.callback === _callback && e.target === this) {
			delete events[i];
			events[i] = null;
			events.splice(i, 1);
		}
	}
}

EventEmitter.prototype.dispatch = function (_eventName, data) {
	var e;
	for (var i = 0; i < events.length; i++) {
		e = events[i];
		if (e.eventName === _eventName && e.target === this) {
			if (e.callback) {
				e.callback(data);
			}
		}
	}
}

Function.prototype.clone = function () {
	var that = this;
	var temp = function temporary() {
		return that.apply(this, arguments);
	};
	for (var key in this) {
		if (this.hasOwnProperty(key)) {
			temp[key] = this[key];
		}
	}
	return temp;
};

EventEmitter.extend = function (ob) {
	var _super = this.prototype;
	var prototype = Object.create(_super);

	function Class(a1, a2, a3, a4, a5, a6) {
		this.ctor(a1, a2, a3, a4, a5, a6);
	}

	prototype.super = prototype.ctor.clone();

	for (var i in ob) {
		prototype[i] = ob[i];
	}

	Class.prototype = prototype;
	Class.extend = this.extend;
	return Class;
}
