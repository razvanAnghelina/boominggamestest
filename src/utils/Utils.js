var Utils = {};

Utils.addZeros = function(nr,total){
    try{
        if(nr.length < total){
            var diff = total-nr.length;
            var zeros = "";
            for(var i=0; i<diff; i++){
                zeros += "0";
            }
            nr = zeros+nr;
        }
    }catch(e){
        throw new Error(e.message);
    }
    return nr;
}