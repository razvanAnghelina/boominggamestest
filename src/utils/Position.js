﻿var Position = function (_x, _y) {
    var ob = {x:0,y:0};

	if (_x !== undefined) {
		ob.x = _x;
	}

	if (_y !== undefined) {
		ob.y = _y;
	}
    
    ob.toString = function(){
        return "{x:"+this.x+",y:"+this.y+"}";
    }
    
    return ob;
};
