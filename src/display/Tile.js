﻿
var Tile = EventEmitter.extend(
    {
		x:0,
        y:0,
        
		fixedPos : null,
		finalPosition : null,
		positionsToMove : 0,
		
		nextPositions : [],
		animPoints : [],
		time : 0,
		doneAnim : false,
		
		type: 0,
		breaked : false,
		
		canMatch : true,
		
		moved : false,
		ignoreBreak : false,
		real_y: 0,
		noHit: 0,
		
		dirToMove : "-",
		
		solutionTypeVertical : false,
		solutionTypeOrizontal : false,
		
		graphic:null,
		border:null,
		bg:null,
		
        ctor:function(color){
            
            this.fixedPos = new Position();
            this.finalPosition = new Position();
            
            this.graphic = new PIXI.Container();
            
            this.bg = new PIXI.Graphics();
            this.bg.beginFill(color,1);
            this.bg.drawRect(0,0,GC.tileSize,GC.tileSize);
            
            this.border = new PIXI.Sprite(getImage("border"));
            this.border.visible = false;
            this.border.pivot.x = this.border.width/2;
            this.border.pivot.y = this.border.height/2;
            
            this.border.x = GC.tileSize/2;
            this.border.y = GC.tileSize/2;
            
            this.graphic.addChild(this.bg);
            this.graphic.addChild(this.border);
            
            this.graphic.pivot.x = GC.tileSize/2;
            this.graphic.pivot.y = GC.tileSize/2;
            
            
        },
        
        showBorder:function(){
            this.border.visible = true;
        },
        
        hideBorder:function(){
            this.border.visible = false;
        },
        
        setColor:function(color){
            this.bg.clear();
            this.bg.beginFill(color,1);
            this.bg.drawRect(0,0,GC.tileSize,GC.tileSize);
        },
        
		hit:function(){
			this.breaked = true;
		}
	});	
