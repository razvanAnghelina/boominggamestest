var LoadingScreen = EventEmitter.extend(
    {
        container:null,
        bar_empty:null,
        bar_full:null,
        
        barWidth:200,
        barHeight:10,
        barEmptyColor:0xcccccc,
        barFullColor:0xcc0000,
        
        ctor:function(container){
            this.container = container;
            
            var bg = new PIXI.Sprite(getImage("loading_bg"));
            
            this.container.addChild(bg);
            
            this.bar_empty = new PIXI.Graphics();
            this.bar_full = new PIXI.Graphics();
            
            this.container.addChild(this.bar_empty);
            this.container.addChild(this.bar_full);
            
            this.bar_empty.beginFill(this.barEmptyColor,1);
            this.bar_empty.drawRect(0,0,this.barWidth,this.barHeight);
            
            this.bar_empty.x = this.bar_full.x = (GC.size.width - this.barWidth)/2;
            this.bar_empty.y = this.bar_full.y = (GC.size.height - this.barHeight)/2 + 70;
            
            var t = this;
            game.on(GameEvents.PRELOAD_PROGRESS,function(p){ t.onProgress(p); });
            
        },
        
        onProgress:function(p){
            
            this.bar_full.clear();
            this.bar_full.beginFill(this.barFullColor,1);
            this.bar_full.drawRect(0,0,this.barWidth * p.progress/100,this.barHeight);
            
        }
        
    });