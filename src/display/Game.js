var Game = EventEmitter.extend(
    {
        container:null,
        renderer:null,
        stage:null,
        canvas:null,
        canvasID:"game",
        scale:1,
        orientation:"",
        loader:null,
        loaded:false,
        gameContainer:null,
        loadingScreen:null,
        imagesFolder:"",
        score:0,
        scoreLabel:null,
        
        tilePuzzle:null,
        tilesContainer:null,
        tilesContainerMask:null,
        
        ctor:function(options){
            for(var i in options){
                if(this[i] !== undefined){
                    this[i] = options[i];
                }
            }
            
            window.getImage = this.getImage;
            window.game = this;
        },
        
        init:function(){
            if(this.container === null){
                this.container = document.body;
            }
            
            this.stage = new PIXI.Container();
            this.loader = new PIXI.loaders.Loader();
            
            window.loader = this.loader;
            
            this.container.innerHTML = '<canvas id="' + this.canvasID + '" tabindex="2"></canvas>';
            this.renderer = PIXI.autoDetectRenderer(GC.size.width, GC.size.height, { backgroundColor: 0x000000, transparent: GC.GAME_IS_TRANSPARENT, view: document.getElementById(this.canvasID) });
            
            this.canvas = this.renderer.view;
            
            var t = this;
            Engine.update = function (dt) { t.Update(dt) };
            Engine.startAnimation();

            var t = this;
            window.addEventListener("resize", function () {
                t.onResize();
            });
             window.addEventListener("orientationchange", function () {
                t.onResize();
            });
            this.onResize();
            
            this.initGameObjects();
            
            var loading = new LoadingScreen(this.loadingScreen);
            
            this.startLoadingSequence();
            
            
        },
        
        initGameObjects:function(){
            this.gameContainer = new PIXI.Container();
            this.stage.addChild(this.gameContainer);
            
            this.loadingScreen = new PIXI.Container();
            this.stage.addChild(this.loadingScreen);
            
        },
        
        onLoadDone:function(){
            this.createGameLayout();
            
            var t = this;
            
            setTimeout(function(){
                TweenLite.to(t.loadingScreen,1,{alpha:0});
            },500);
            
        },
        
        createGameLayout:function(){
            var bg = new PIXI.Sprite(getImage("background"));
            this.gameContainer.addChild(bg);
            
            this.tilesContainer = new PIXI.Container();
            this.gameContainer.addChild(this.tilesContainer);
            
            this.tilesContainerMask = new PIXI.Graphics();
            
            this.gameContainer.addChild(this.tilesContainerMask);
            this.tilesContainer.mask = this.tilesContainerMask;
            
            this.scoreLabel = new PIXI.Text();
            this.scoreLabel.style.fontFamily = "Roboto";
            this.scoreLabel.style.fontSize = 30;
            this.scoreLabel.style.fill = 0xffffff;
            this.scoreLabel.anchor.x = .5;
            
            this.scoreLabel.x = 172;
            this.scoreLabel.y = 170;
            
            this.gameContainer.addChild(this.scoreLabel);
            
            this.tilePuzzle = new TilePuzzle();
            this.tilePuzzle.container = this.tilesContainer;
            this.tilePuzzle.container.x = 380;
            this.tilePuzzle.container.y = 112;
			this.tilePuzzle.create();
            
            var t = this;
			this.tilePuzzle.on(GameEvents.TILE_BREAKED,function(scoreToAdd){ t.onTileBreaked(scoreToAdd); })
            
            this.tilesContainerMask.x = this.tilePuzzle.container.x - GC.tileSize/2;
            this.tilesContainerMask.y = this.tilePuzzle.container.y - GC.tileSize/2;
            this.tilesContainerMask.beginFill(0x000000,1);
            this.tilesContainerMask.drawRect(0,0,GC.cols*(GC.tileSize+GC.padding),GC.rows*(GC.tileSize+GC.padding));
            
            
            this.updateScore();
            
        },
        
        onTileBreaked:function(scoreToAdd){
            this.score += scoreToAdd;
            this.updateScore();
        },
        
        updateScore:function(){
            this.scoreLabel.text = Utils.addZeros(Math.round(this.score).toString(),6);
        },
        
        onResize:function(){
            var new_scale = 1;
            
            var STAGE_WIDTH = window.innerWidth;
            var STAGE_HEIGHT = window.innerHeight;

            var rap = GC.size.width / GC.size.height;
            var ww = STAGE_WIDTH;
            var hh = STAGE_WIDTH / rap;

            var _sw = STAGE_WIDTH;
            var _sh = STAGE_HEIGHT;

            this.orientation = _sw > _sh ? GC.ORIENTATION_LIST.LANDSCAPE : GC.ORIENTATION_LIST.PORTRAIT;

            var rap2 = _sw / _sh;

            var pixelRatio = 1;

            if (window.devicePixelRatio > 1) {
                pixelRatio = window.devicePixelRatio;
            }

            this.canvas.width = _sw * pixelRatio;
            this.canvas.height = _sh * pixelRatio;
            this.canvas.style.width = _sw + 'px';
            this.canvas.style.height = _sh + 'px';

            var nh = _sw / rap;
            new_scale = _sw / GC.size.width;

            if (this.orientation == GC.ORIENTATION_LIST.LANDSCAPE && nh > _sh) {
                new_scale = _sh / GC.size.height;
            }

            this.stage.scale.x = (new_scale * pixelRatio);
            this.stage.scale.y = (new_scale * pixelRatio);
            this.stage.initScale = (new_scale * pixelRatio);
            this.stage.x = (this.canvas.width - GC.size.width * this.stage.scale.x) / 2;
            this.stage.initX = (this.canvas.width - GC.size.width * this.stage.scale.x) / 2;
            this.stage.y = 0;
            this.stage.initY = 0;

            this.renderer.resize(this.canvas.width, this.canvas.height);
            this.scale = new_scale;
            this.dispatch(GameEvents.RESIZE, { screenWidth: STAGE_WIDTH, screenHeight: STAGE_HEIGHT, gameScale: new_scale, pixelRatio: pixelRatio, orientation: this.orientation });
            
        },
        
        Update: function (dt) {
            this.renderer.render(this.stage);
            this.dispatch(GameEvents.UPDATE);
        },
        
        getImage: function (tex) {
            var t = loader.resources[tex];
            if (t !== undefined) {
                return t.texture;
            }
            
            
            try{
                 t = PIXI.Texture.fromFrame(tex + '.png');
            }catch(e){
                try{
                    t = PIXI.Texture.fromImage(game.imagesFolder + tex + '.png'); 
                }catch(e){
                    Log("texture not found: ",tex);
                }
            }
            
            return t;
        },
        
        startLoadingSequence : function(){
            
            for (var i in GC.assets) {
                this.loader.add(i, GC.assets[i]);
            }

            var t = this;
            this.loader.on("progress", function (e) { t.dispatch(GameEvents.PRELOAD_PROGRESS, e); });
            this.loader.once("complete", function (e) { t.loaded = true; t.dispatch(GameEvents.PRELOAD_COMPLETE); t.onLoadDone();  });
            this.loader.load();  
        }
        
    });