﻿
var TilePuzzle = EventEmitter.extend(
    {
        
        tiles:[],
		container:null,
		tileSize:GC.tileSize,
		padding:GC.padding,
		colors:[0x0099CC, 0x99FF00, 0xFF33FF, 0xFFFF33, 0xcc0000],
		
		rows:GC.rows,
		cols:GC.cols,
        
        removeTiles:[],
		animTiles:[],
		
		firstSelectedTile:null,
		secondSelectedTile:null,
		
		canMove:true,

		firstSelectedTilePos:null,
		secondSelectedTilePos:null,
        
        tilesToMove:[],
        
        // game constructor
        ctor:function(options){
            for(var i in options){
                if(this[i] !== undefined){
                    this[i] = options[i];
                }
            }
        },
        
        create:function(){
			var i;
			var j;
            
            this.firstSelectedTilePos = new Position();
		    this.secondSelectedTilePos = new Position();
			
			var type = 0;
			
            window.puzzleEngine = this;
            
			var fixedFound = false;
            
            addTouchEvents(this.container);
            var _this = this;
            
            this.container.onClick = function(e){
                _this.onTileDown(e);
            }
			
			for(i = 0; i<this.rows; i++){
				this.tiles[i] = [];
                
				for(j = 0; j<this.cols; j++){
					
                    
                    type = this.getRandomType();
                    
					var t = new Tile(this.colors[type]);
					this.tiles[i][j] = t;
						
					t.type = type;
					
					this.container.addChild(t.graphic);
					t.graphic.width = this.tileSize;
					t.graphic.height = this.tileSize;
					t.graphic.x = j * (this.tileSize+this.padding);
					t.graphic.y = (i - this.rows) * (this.tileSize+this.padding);
					t.graphic.tile = t;
					t.x = j;
					t.y = i;
					t.fixedPos.x = j;
					t.fixedPos.y = i;
                    t.graphic.alpha = 0;
				}
			}
            
			this.shuffleBoard();
            
            setTimeout(function(){
               _this.showInitialAnimation(); 
            },1000);
            
		},
        
        getRandomType:function(){
             return Math.floor(Math.random()*this.colors.length);
        },
        
        showInitialAnimation:function(){
            var tile,i,j;
            var t = this;
            
            var timeline = new TimelineLite({onComplete:function(){ t.canMove = true; }});
            this.canMove = false;
            
            for(i = 0; i<this.rows; i++){
				for(j = 0; j<this.cols; j++){
                    tile = this.tiles[i][j];
                    timeline.to(tile.graphic,1,{alpha:1,y: i * (this.tileSize+this.padding),ease:Quad.easeOut},(this.rows-i+j)/30);
                }
            }
        },
		
		shuffleBoard:function(){
			var sols = this.getMatches();
			var i;
			var j;
			var type = 0;
			while(sols.length > 0){
				for(i=0; i<this.tiles.length; i++){
					for(j=0; j<this.tiles[0].length; j++){
						type = this.getRandomType();
						this.tiles[i][j].type = type;
						this.tiles[i][j].setColor(this.colors[type]);
					}
				}
				sols = this.getMatches();
			}
			
			for(i=0; i<this.tiles.length; i++){
				for(j=0; j<this.tiles[0].length; j++){
					this.tiles[i][j].setColor(this.colors[this.tiles[i][j].type]);
				}
			}
			
		},
		
		getMatches:function(){
			var i = 0;
			var j = 0;
			var k = 0;
			
			var count = 0;
			var lastType = 0;
			var t;
			
			var _type = 0;
			var solutions = [];
			var sol;
            
			/// verificare pe orizontala
			for(i=0; i<this.tiles.length; i++){
				lastType = 0;
				sol = [];
				_type = 0;
				for(j=0; j<this.tiles[0].length; j++){
					t = this.tiles[i][j];
					if(t.breaked){
						////////
						if(sol.length >= 3){
							solutions.push(sol);
						}
						sol = [];
						//////////
						continue;
					}
					_type = t.type;
					if(_type != lastType || j == this.tiles[0].length - 1){
						if(_type == lastType && j == this.tiles[0].length - 1){
							sol.push(t);
						}
						lastType = _type;						
						if(sol.length >= 3){
							solutions.push(sol);
						}
						sol = [];
					}
					sol.push(t);
				}
			}
			/////// end on this.rows
			////////////////////////////////////////
			
			/// verificare pe verticala
			for(i=0; i<this.tiles[0].length; i++){
				lastType = 0;
				sol = [];
				_type = 0;
				for(j=0; j<this.tiles.length; j++){
					t = this.tiles[j][i];
					if(t.breaked){
						////////
						if(sol.length >= 3){
							solutions.push(sol);
							for(k=0; k<sol.length; k++){
								sol[k].solutionTypeVertical = true;
							}
						}
						//////////
						sol = [];
						continue;
					}
					_type = t.type;
					if(_type != lastType || j == this.tiles.length - 1){
						if(_type == lastType && j == this.tiles.length - 1){
							sol.push(t);
							
						}
						lastType = _type;
						if(sol.length >= 3){
							solutions.push(sol);
							for(k=0; k<sol.length; k++){
								sol[k].solutionTypeVertical = true;
							}
						}
						sol = [];
					}
					sol.push(t);
				}
			}
			/////// end on columns
			////////////////////////////////////////
			
			return solutions;
		},
		
		onTileDown:function(e){
            
			if(!this.canMove){
				return;
			}
            
            var position = this.container.toLocal(e.data.global);
            position.x += this.tileSize/2;
            position.y += this.tileSize/2;
            
            var _x = Math.floor(position.x / (GC.tileSize + GC.padding));
            var _y = Math.floor(position.y / (GC.tileSize + GC.padding));
            
            if(_x < 0){
                _x = 0;
            }
            
            if(_y < 0){
                _y = 0;
            }
            
            if(_x > this.cols.length - 1){
                _x = this.cols.length - 1;
            }
            
            if(_y > this.rows.length - 1){
                _y = this.rows.length - 1;
            }
            
			var t = this.tiles[_y][_x];
			t.graphic.parent.setChildIndex(t.graphic,t.graphic.parent.numChildren-1);
            
			if(t == this.firstSelectedTile){
				return;
			}
			
			t.showBorder();
			
			if(this.firstSelectedTile == null){
				this.firstSelectedTile = t;
				this.firstSelectedTilePos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
			}else if(this.secondSelectedTile == null){
				if(this.isNeighbor(t,this.firstSelectedTile)){
					this.secondSelectedTile = t;
					this.secondSelectedTilePos = new Position(this.secondSelectedTile.x,this.secondSelectedTile.y);
					this.swapSelectedTiles();
				}else{
					this.firstSelectedTile.hideBorder();
					this.firstSelectedTile = t;
					this.firstSelectedTilePos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
				}
			}
		},
		
		isNeighbor:function(t1,t2){
			return (Math.abs(t1.x - t2.x) < 2 && Math.abs(t1.y - t2.y) < 1) || (Math.abs(t1.y - t2.y) < 2 && Math.abs(t1.x - t2.x) < 1);
		},
		
		swapSelectedTiles : function(){
			this.canMove = false;
			var t1pos = new Position(this.firstSelectedTile.x,this.firstSelectedTile.y);
            
            
			this.firstSelectedTile.x = this.secondSelectedTile.x;
			this.firstSelectedTile.y = this.secondSelectedTile.y;
			
			this.tiles[this.firstSelectedTile.y][this.firstSelectedTile.x] = this.firstSelectedTile;
			
			this.secondSelectedTile.x = t1pos.x;
			this.secondSelectedTile.y = t1pos.y;
			
			this.tiles[this.secondSelectedTile.y][this.secondSelectedTile.x] = this.secondSelectedTile;
			
            var t = this;
			TweenMax.to(this.firstSelectedTile.graphic,.4,{x:this.secondSelectedTile.graphic.x,y:this.secondSelectedTile.graphic.y});
			TweenMax.to(this.secondSelectedTile.graphic,.5,{x:this.firstSelectedTile.graphic.x,y:this.firstSelectedTile.graphic.y,onComplete:function(){ t.checkTilesMove(); }});
            
		},
		
		
		hitTile : function(t){
			if(t.ignoreBreak || t.breaked){
				return;
			}
			t.hit();
			if(t.breaked && t.graphic != null){
				this.animTiles.push(t.graphic);
				this.removeTiles.push(t);
			}
		},

		
		checkTilesMove : function(){

			var sols = this.getMatches();
			
			if(sols.length == 0){
				if(this.firstSelectedTile != null){
					TweenMax.to(this.firstSelectedTile.graphic,.4,{x:this.secondSelectedTile.graphic.x,y:this.secondSelectedTile.graphic.y});
					TweenMax.to(this.secondSelectedTile.graphic,.4,{x:this.firstSelectedTile.graphic.x,y:this.firstSelectedTile.graphic.y});
					
					this.firstSelectedTile.x = this.firstSelectedTile.fixedPos.x;
					this.firstSelectedTile.y = this.firstSelectedTile.fixedPos.y;
					this.tiles[this.firstSelectedTile.y][this.firstSelectedTile.x] = this.firstSelectedTile;
					
					this.secondSelectedTile.x = this.secondSelectedTile.fixedPos.x;
					this.secondSelectedTile.y = this.secondSelectedTile.fixedPos.y;
					this.tiles[this.secondSelectedTile.y][this.secondSelectedTile.x] = this.secondSelectedTile;
				}
				
				this.canMove = true;
				
			}else{
				this.destroySolutions(sols);
			}
			
			if(this.firstSelectedTile != null){
				this.firstSelectedTile.hideBorder();
				this.secondSelectedTile.hideBorder();
			}
			
			this.firstSelectedTile = null;
			this.secondSelectedTile = null;
		},
            
		destroySolutions : function (sols){
			var i;
			var j;
			
			this.animTiles.length = 0;
			
			for(i=0; i<sols.length; i++){
				var index = 0;

				/////////////////
				
				for(j=0; j<sols[i].length; j++){
					if(this.animTiles.indexOf(sols[i][j].graphic) == -1){
						sols[i][j].hit();
				
						if(sols[i][j].breaked && this.animTiles.indexOf(sols[i][j].graphic) == -1){
							this.animTiles.push(sols[i][j].graphic);
							this.removeTiles.push(sols[i][j]);
						}
					}
				}
			}
			
			this.showDestroyAnimation();
			
		},
		
		showDestroyAnimation : function(){
            var t = this;
            var timeline = new TimelineLite({onComplete:function(){ t.refillEmptySpaces(); }});
            for(var i=0; i<this.animTiles.length; i++){
                timeline.to(this.animTiles[i],.3,{alpha:0,ease:Quad.easeOut},0);
                timeline.to(this.animTiles[i].scale,.3,{x:0,y:0,ease:Quad.easeOut},0);
                this.dispatch(GameEvents.TILE_BREAKED,50);
            }
            
            this.animTiles.length = 0;
		},
		
		
		refillEmptySpaces : function(){
            
			var i = 0;
			var j;
			var count = 0;
			
            var _x = 0, _y = 0;
            var emptyTiles = [];
            
            for(_x=0; _x<this.cols; _x++){
                var moveDown = 0;
                emptyTiles[_x] = [];
                
                for(_y=this.rows-1; _y>=0; _y--){
                    if(this.tiles[_y][_x].breaked){
                        moveDown++;
                        emptyTiles[_x].push(_y);
                    }else if(moveDown > 0){
                        this.tilesToMove.push(this.tiles[_y][_x]);
                        this.tiles[_y][_x].y += moveDown;
                        this.tiles[_y][_x].fixedPos.y = this.tiles[_y][_x].y;
                    }
                    
                }
            }
            
            for(i=0; i<this.cols; i++){
                for(j=0; j<emptyTiles[i].length; j++){
                    _y = emptyTiles[i][j];
                    _x = i;
                    
                    this.tiles[_y][_x].breaked = false;
                    this.tiles[_y][_x].graphic.alpha = 1;
                    this.tiles[_y][_x].graphic.scale.x = this.tiles[_y][_x].graphic.scale.y = 1;
                    var new_type = this.getRandomType();
                    this.tiles[_y][_x].graphic.y = -(j+1)*(this.tileSize+this.padding);
                    this.tiles[_y][_x].type = new_type;
                    this.tiles[_y][_x].setColor(this.colors[new_type]);
                    this.tilesToMove.push(this.tiles[_y][_x]);
                    this.tiles[_y][_x].y = emptyTiles[i].length - 1 - j;
                    this.tiles[_y][_x].fixedPos.y = this.tiles[_y][_x].y;
                }
            }
            
			this.removeTiles.length = 0;
            
			if(this.tilesToMove.length > 0){
				this.showRefillAnimation();
			}else{
				this.checkIfWeCanMoveAgain();
			}
			
			
		},
		
        
		showRefillAnimation : function(){
            var t = this;
            var timeline = new TimelineLite({onComplete:function(){ t.checkIfWeCanMoveAgain(); }});
            for(var i=0; i<this.tilesToMove.length; i++){
                timeline.to(this.tilesToMove[i].graphic,.3,{y:this.tilesToMove[i].y*(this.tileSize+this.padding),ease:Quad.easeOut},0);
                this.tiles[this.tilesToMove[i].y][this.tilesToMove[i].x] = this.tilesToMove[i];
            }
            
            this.tilesToMove.length = 0;
		},
		
		checkIfWeCanMoveAgain : function(){
			var i = 0;
			var j = 0;
			
			this.firstSelectedTilePos.x = this.firstSelectedTilePos.y = this.secondSelectedTilePos.x = this.secondSelectedTilePos.y = -1;
			
			this.tilesToMove.length = 0;
			this.firstSelectedTile = this.secondSelectedTile = null;
			
			this.checkTilesMove();
		}
    }
);
    
