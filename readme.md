Hello! 

Thank you for downloading and playing my test game. The game is created using PIXI.js and it won't run locally (unless you run it on a local apache server like XAMPP etc). 
You can test it easier on this link:  http://webdesignmedia.ro/boomingGamesTest/public/index.html

To change the game, you can modify the files in the src/ folder.
The game can be run from the files public/index.html and files/index_test.html (index.html sources a minified / compiled version of the code, 
use index_test.html to test the changes imediatelly).

In order to compile the code, I've used gulp.js -> https://gulpjs.com/

How the code is structured:
	• src/main.js  -> this file inits the game code
	• src/display/Game.js  ->  the game framework is here; here we manage the game stage / resize and the game layout, loading and the init of the TilePuzzle object etc
	• src/display/TilePuzzle.js  ->  this file manages the tile matching game with all the functions for matching the colors and refilling of the spaces
	• src/display/tile.js  ->  a simple class to keep the logic of one tile object.
	
The game settings should be changed via src/config/Config.js  (rows, cols, game assets etc)

Thank you!